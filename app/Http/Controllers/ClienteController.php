<?php

namespace App\Http\Controllers;

use App\Models\Cliente;
use Illuminate\Http\Request;

class ClienteController extends Controller
{
    public function index(){
        // tengo que leer todos los clientes y mandarlos a la vista
       // $clientes=Cliente::all();
       // vamos a mandar los lientes paginados
        $clientes=Cliente::paginate(6);
        

        return view('cliente.index',[
            "clientes"=>$clientes // le paso a la vista el array de clientes
        ]);
    }
    public function show($id){
        // tengo que leer todos los clientes y mandarlos a la vista
       // $clientes=Cliente::all();
       // vamos a mandar los lientes paginados
       $cliente=Cliente::find($id);
        return view('cliente.show',[
            "cliente"=>$cliente // le paso a la vista el array de clientes
        ]);
    }
    public function create(){
        return view('cliente.create');
    }

    // cuando en el formulario crar registro
    // pulso el boton de insertar
    // me llega este metodo
    public function store(Request $request){
        //validar
        $request->validate([
            'nombre'=>'required',
            'email'=>'required',
            ]
        );
        // asignacion masiva
       $cliente=Cliente::create($request->all());
        // sin asignacion masiva
        // $cliente= new Cliente();
        // $cliente->nombre=$request->input('nombre'); // $request->nombre;
        // $cliente->email=$request->input('email'); // $request->email;
        // $cliente->save();
        // redireccionar a la vista index
        // podemos redireccionar a la vista show
        

         return redirect()
         ->route('cliente.show', $cliente->id)
         ->with('mensaje', 'Registro creado correctamente');

    }
    // cargar el formulario de editar
    public function edit($id){
        // recupero los datos del cliente a editar
        $cliente=Cliente::find($id);
        // mostrar el formulario para editar el cliente
        return view('cliente.edit',[
            'cliente'=>$cliente
        ]);
    
    }
/// cuando en el formulario de editar pulso el boton de actualizar
    public function update(Request $request, Cliente $cliente){
// validamos los datos

$request->validate([
    'nombre'=>'required',
    'email'=>'required',
    ]);
// actualizar
$cliente->update($request->all());
// redireccionar a la vista show
return redirect()
->route('cliente.show', $cliente->id)
->with('mensaje', 'Registro actualizado correctamente');
    }

    public function destroy(Cliente $cliente){
        // eliminamos el cliente
        $cliente->delete();
        // redicionamos a la vista index
        return redirect()
        ->route('cliente.index')
        ->with('mensaje', 'Registro eliminado correctamente');
    }
}