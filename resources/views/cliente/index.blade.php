@extends('layouts.main')

@section('titulo', 'Listado')

@section('cabecera')
    <h1> Listado de clientes</h1>
    @parent
@endsection

@section('contenido')
@if(@session('mensaje'))
<div class="row mt-3">
    <div class="alert alert-info">
        {{session('mensaje')}}
    </div>

</div>
@endif

    <div class="row mt-3">
        @foreach ($clientes as $cliente)
            <div class="col-lg-5 m-1">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">{{ $cliente->id }}</h5>
                        <p class="card-text">Nombre : {{ $cliente->nombre }}</p>
                        <p class="card-text">Email : {{ $cliente->email }}</p>
                        <div>
                            <a href="{{ route('cliente.show', $cliente->id) }}" class="btn btn-primary">Ver</a>
                            <a href="{{ route('cliente.edit', $cliente->id) }}" class="btn btn-primary">Editar</a>
                            <div class="vertical-align-bottom d-flex justify-content-end">
                                <form action="{{route('cliente.destroy', $cliente)}}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn btn-danger">Eliminar</button>
                                </form>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
    <div class="row mt-3">
        {{ $clientes->links() }}
    </div>
@endsection
