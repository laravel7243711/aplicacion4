@extends('layouts.main')

@section('titulo', 'Mostrar')

@section('cabecera')
    <h1> Datos del cliente {{ $cliente->id }}</h1>
    @parent
@endsection

@section('contenido')
    @if (session('mensaje'))
    
    <div class="row m-2">
        <div class="alert alert-info">
        {{ session('mensaje') }}
    </div>
    @endif

    <div class="row mt-3">
        <div class="col-lg-5 m-1">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">
                        {{ $cliente->id }}
                    </h5>
                    <p class="card-text">
                        Nombre: {{ $cliente->nombre }}
                    </p>
                    <p class="card-text">
                        email: {{ $cliente->email }}
                    </p>
                    <div>
                        <a href="{{ route('cliente.edit', $cliente->id) }}" class="btn btn-primary">Editar</a>
                        <form action="{{route('cliente.destroy', $cliente)}}" method="POST">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-danger">Eliminar</button>
                        </form>
                        
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
@endsection
