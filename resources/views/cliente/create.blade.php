@extends('layouts.main')

@section('titulo', 'Nuevo')

@section('cabecera')
    <h1> Nuevo registros</h1>
    @parent
@endsection

@section('contenido')
    <div class="row mt-3">
        <div class="col-lg-5 m-1">
            <form action="{{ route('cliente.store') }}" method="post">
                @csrf
                <div class="mb-3">
                    <label for="nombre" class="form-label">Nombre</label>
                    <input type="text" class="form-control" id="nombre" name="nombre">
                </div>
                <div class="mb-3">
                    <label for="email" class="form-label">Email</label>
                    <input type="email" class="form-control" id="email" name="email">
                </div>

                <button type="submit" class="btn btn-primary">Insertar</button>

        </div>
    @endsection
